#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
hPosition = 0
aim=0
depth = 0
for d in data:
	key, value = d.split(" ")
	if key == "forward":
		hPosition += int(value)
		depth += aim*int(value)
	elif key == "down": aim += int(value)
	elif key == "up": aim -= int(value)
print(hPosition*depth)
