#!/bin/bash
for f in examples/in*; do 
    OUT=`python code.py $f`
    ANS=`cat ${f//in/out}`
    if [[ $OUT = $ANS ]]; then
        echo -e "Example ${f//[^0-9]/} OK\n-->$OUT";
    else
        echo -e "Example ${f//[^0-9]/} ERROR\n-->$OUT\nExpected answer:\n-->$ANS";
    fi
done
