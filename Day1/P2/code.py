#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
data = list(map(int,data))

increments = 0

for i in range(2,len(data)-1):
    if (data[i+1]>data[i-2]): increments += 1
print(increments)
