#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
grad = len(data[0].rstrip('\n'))
acc = [ 0 for i in range(grad)]
for d in data:
	strBin = list(d.rstrip('\n'))
	for i in range(grad):
		acc[i] += int(strBin[i])
num = 0
for i in range(grad):
	num += (2*acc[grad-i-1]/len(data))*(2**i)
print(((2**grad)-1-num)*num)
