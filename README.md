# Advent Of Code 2021\nAll AdventOfCode(2021) problems and solutions in Python\n\n# Status\nIn progress...\n\n\n\n--- Day 1: Sonar Sweep ---
You're minding your own business on a ship at sea when the overboard alarm goes off! You rush to see if you can help. Apparently, one of the Elves tripped and accidentally sent the sleigh keys flying into the ocean!

Before you know it, you're inside a submarine the Elves keep ready for situations like this. It's covered in Christmas lights (because of course it is), and it even has an experimental antenna that should be able to track the keys if you can boost its signal strength high enough; there's a little meter that indicates the antenna's signal strength by displaying 0-50 stars.

Your instincts tell you that in order to save Christmas, you'll need to get all fifty stars by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

As the submarine drops below the surface of the ocean, it automatically performs a sonar sweep of the nearby sea floor. On a small screen, the sonar sweep report (your puzzle input) appears: each line is a measurement of the sea floor depth as the sweep looks further and further away from the submarine.

For example, suppose you had the following report:

199
200
208
210
200
207
240
269
260
263
This report indicates that, scanning outward from the submarine, the sonar sweep found depths of 199, 200, 208, 210, and so on.

The first order of business is to figure out how quickly the depth increases, just so you know what you're dealing with - you never know if the keys will get carried into deeper water by an ocean current or a fish or something.

To do this, count the number of times a depth measurement increases from the previous measurement. (There is no measurement before the first measurement.) In the example above, the changes are as follows:

199 (N/A - no previous measurement)
200 (increased)
208 (increased)
210 (increased)
200 (decreased)
207 (increased)
240 (increased)
269 (increased)
260 (decreased)
263 (increased)
In this example, there are 7 measurements that are larger than the previous measurement.

How many measurements are larger than the previous measurement?\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
data = list(map(int,data))
increments = 0
for i in range(len(data)-1):
	if (data[i+1]>data[i]): increments += 1
print(increments)\n\n## Solution1475--- Part Two ---
Considering every single measurement isn't as useful as you expected: there's just too much noise in the data.

Instead, consider sums of a three-measurement sliding window. Again considering the above example:

199  A      
200  A B    
208  A B C  
210    B C D
200  E   C D
207  E F   D
240  E F G  
269    F G H
260      G H
263        H
Start by comparing the first and second three-measurement windows. The measurements in the first window are marked A (199, 200, 208); their sum is 199 + 200 + 208 = 607. The second window is marked B (200, 208, 210); its sum is 618. The sum of measurements in the second window is larger than the sum of the first, so this first comparison increased.

Your goal now is to count the number of times the sum of measurements in this sliding window increases from the previous sum. So, compare A with B, then compare B with C, then C with D, and so on. Stop when there aren't enough measurements left to create a new three-measurement sum.

In the above example, the sum of each three-measurement window is as follows:

A: 607 (N/A - no previous sum)
B: 618 (increased)
C: 618 (no change)
D: 617 (decreased)
E: 647 (increased)
F: 716 (increased)
G: 769 (increased)
H: 792 (increased)
In this example, there are 5 sums that are larger than the previous sum.

Consider sums of a three-measurement sliding window. How many sums are larger than the previous sum?\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
data = list(map(int,data))

increments = 0

for i in range(2,len(data)-1):
    if (data[i+1]>data[i-2]): increments += 1
print(increments)\n\n## Solution1516\n\n--- Day 2: Dive! ---
Now, you need to figure out how to pilot this thing.

It seems like the submarine can take a series of commands like forward 1, down 2, or up 3:

forward X increases the horizontal position by X units.
down X increases the depth by X units.
up X decreases the depth by X units.
Note that since you're on a submarine, down and up affect your depth, and so they have the opposite result of what you might expect.

The submarine seems to already have a planned course (your puzzle input). You should probably figure out where it's going. For example:

forward 5
down 5
forward 8
up 3
down 8
forward 2
Your horizontal position and depth both start at 0. The steps above would then modify them as follows:

forward 5 adds 5 to your horizontal position, a total of 5.
down 5 adds 5 to your depth, resulting in a value of 5.
forward 8 adds 8 to your horizontal position, a total of 13.
up 3 decreases your depth by 3, resulting in a value of 2.
down 8 adds 8 to your depth, resulting in a value of 10.
forward 2 adds 2 to your horizontal position, a total of 15.
After following these instructions, you would have a horizontal position of 15 and a depth of 10. (Multiplying these together produces 150.)

Calculate the horizontal position and depth you would have after following the planned course. What do you get if you multiply your final horizontal position by your final depth?\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
hPosition = 0
depth = 0
for d in data:
	key, value = d.split(" ")
	if key == "forward": hPosition += int(value)
	elif key == "down": depth += int(value)
	elif depth >= int(value): depth -= int(value)
	else: depth = 0
print(hPosition*depth)\n\n## Solution1451208--- Part Two ---
Based on your calculations, the planned course doesn't seem to make any sense. You find the submarine manual and discover that the process is actually slightly more complicated.

In addition to horizontal position and depth, you'll also need to track a third value, aim, which also starts at 0. The commands also mean something entirely different than you first thought:

down X increases your aim by X units.
up X decreases your aim by X units.
forward X does two things:
It increases your horizontal position by X units.
It increases your depth by your aim multiplied by X.
Again note that since you're on a submarine, down and up do the opposite of what you might expect: "down" means aiming in the positive direction.

Now, the above example does something different:

forward 5 adds 5 to your horizontal position, a total of 5. Because your aim is 0, your depth does not change.
down 5 adds 5 to your aim, resulting in a value of 5.
forward 8 adds 8 to your horizontal position, a total of 13. Because your aim is 5, your depth increases by 8*5=40.
up 3 decreases your aim by 3, resulting in a value of 2.
down 8 adds 8 to your aim, resulting in a value of 10.
forward 2 adds 2 to your horizontal position, a total of 15. Because your aim is 10, your depth increases by 2*10=20 to a total of 60.
After following these new instructions, you would have a horizontal position of 15 and a depth of 60. (Multiplying these produces 900.)

Using this new interpretation of the commands, calculate the horizontal position and depth you would have after following the planned course. What do you get if you multiply your final horizontal position by your final depth?\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
hPosition = 0
aim=0
depth = 0
for d in data:
	key, value = d.split(" ")
	if key == "forward":
		hPosition += int(value)
		depth += aim*int(value)
	elif key == "down": aim += int(value)
	elif key == "up": aim -= int(value)
print(hPosition*depth)\n\n## Solution1620141160\n\n--- Day 3: Binary Diagnostic ---
The submarine has been making some odd creaking noises, so you ask it to produce a diagnostic report just in case.

The diagnostic report (your puzzle input) consists of a list of binary numbers which, when decoded properly, can tell you many useful things about the conditions of the submarine. The first parameter to check is the power consumption.

You need to use the binary numbers in the diagnostic report to generate two new binary numbers (called the gamma rate and the epsilon rate). The power consumption can then be found by multiplying the gamma rate by the epsilon rate.

Each bit in the gamma rate can be determined by finding the most common bit in the corresponding position of all numbers in the diagnostic report. For example, given the following diagnostic report:

00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the most common bit is 1, the first bit of the gamma rate is 1.

The most common second bit of the numbers in the diagnostic report is 0, so the second bit of the gamma rate is 0.

The most common value of the third, fourth, and fifth bits are 1, 1, and 0, respectively, and so the final three bits of the gamma rate are 110.

So, the gamma rate is the binary number 10110, or 22 in decimal.

The epsilon rate is calculated in a similar way; rather than use the most common bit, the least common bit from each position is used. So, the epsilon rate is 01001, or 9 in decimal. Multiplying the gamma rate (22) by the epsilon rate (9) produces the power consumption, 198.

Use the binary numbers in your diagnostic report to calculate the gamma rate and epsilon rate, then multiply them together. What is the power consumption of the submarine? (Be sure to represent your answer in decimal, not binary.)\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code
grad = len(data[0].rstrip('\n'))
acc = [ 0 for i in range(grad)]
for d in data:
	strBin = list(d.rstrip('\n'))
	for i in range(grad):
		acc[i] += int(strBin[i])
num = 0
for i in range(grad):
	num += (2*acc[grad-i-1]/len(data))*(2**i)
print(((2**grad)-1-num)*num)\n\n## Solution3374136\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n--- Day 4: Giant Squid ---
You're already almost 1.5km (almost a mile) below the surface of the ocean, already so deep that you can't see any sunlight. What you can see, however, is a giant squid that has attached itself to the outside of your submarine.

Maybe it wants to play bingo?

Bingo is played on a set of boards each consisting of a 5x5 grid of numbers. Numbers are chosen at random, and the chosen number is marked on all boards on which it appears. (Numbers may not appear on all boards.) If all numbers in any row or any column of a board are marked, that board wins. (Diagonals don't count.)

The submarine has a bingo subsystem to help passengers (currently, you and the giant squid) pass the time. It automatically generates a random order in which to draw numbers and a random set of boards (your puzzle input). For example:

7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no winners, but the boards are marked as follows (shown here adjacent to each other to save space):

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are still no winners:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
Finally, 24 is drawn:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
At this point, the third board wins because it has at least one complete row or column of marked numbers (in this case, the entire top row is marked: 14 21 17 24 4).

The score of the winning board can now be calculated. Start by finding the sum of all unmarked numbers on that board; in this case, the sum is 188. Then, multiply that sum by the number that was just called when the board won, 24, to get the final score, 188 * 24 = 4512.

To guarantee victory against the giant squid, figure out which board will win first. What will your final score be if you choose that board?\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n--- Day 5: Hydrothermal Venture ---
You come across a field of hydrothermal vents on the ocean floor! These vents constantly produce large, opaque clouds, so it would be best to avoid them if possible.

They tend to form in lines; the submarine helpfully produces a list of nearby lines of vents (your puzzle input) for you to review. For example:

0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
Each line of vents is given as a line segment in the format x1,y1 -> x2,y2 where x1,y1 are the coordinates of one end the line segment and x2,y2 are the coordinates of the other end. These line segments include the points at both ends. In other words:

An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.
For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.

So, the horizontal and vertical lines from the above list would produce the following diagram:

.......1..
..1....1..
..1....1..
.......1..
.112111211
..........
..........
..........
..........
222111....
In this diagram, the top left corner is 0,0 and the bottom right corner is 9,9. Each position is shown as the number of lines which cover that point or . if no line covers that point. The top-left pair of 1s, for example, comes from 2,2 -> 2,1; the very bottom row is formed by the overlapping lines 0,9 -> 5,9 and 0,9 -> 2,9.

To avoid the most dangerous areas, you need to determine the number of points where at least two lines overlap. In the above example, this is anywhere in the diagram with a 2 or larger - a total of 5 points.

Consider only horizontal and vertical lines. At how many points do at least two lines overlap?\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution5\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution\n\n## Code#!/usr/bin/python

import sys

# Read data
data = ""
if (len(sys.argv) == 2):
    _f = open(sys.argv[1],"r")
    data = _f.readlines()
    _f.close()

# Code\n\n## Solution
