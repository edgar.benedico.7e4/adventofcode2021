#!/bin/bash
for d in {1..25}; do 
    mkdir "Day$d"
    for p in {1..2}; do
        mkdir "../Day$d/P$p";
        touch "../Day$d/P$p/statement.txt" "../Day$d/P$p/input.txt" "../Day$d/P$p/output.txt" "../Day$d/P$p/code.py";
        cp "./code.py" "../Day$d/P$p/code.py"
        cp "./run.sh" "../Day$d/P$p/run.sh"
        cp "./tests.sh" "../Day$d/P$p/tests.sh"
        mkdir "../Day$d/P$p/examples";
        touch "../Day$d/P$p/examples/in1.txt" "../Day$d/P$p/examples/out1.txt";
    done
done
