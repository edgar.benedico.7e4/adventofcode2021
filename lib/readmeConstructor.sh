#!/bin/bash

TXT=""

# Head
TXT+=$"# Advent Of Code 2021\nAll AdventOfCode(2021) problems and solutions in Python\n\n"
TXT+=$"# Status\nIn progress...\n\n"
# Body
for d in {1..25};do
	TXT+="\n\n"
	for p in {1..2};do
		TXT+=`cat "../Day$d/P$p/statement.txt"`
		TXT+=$"\n\n## Code"
		TXT+=`cat "../Day$d/P$p/code.py"`
		TXT+=$"\n\n## Solution"
		TXT+=`cat "../Day$d/P$p/output.txt"`
	done
done
# Footer

# Code
echo "$TXT" > ../README.md
